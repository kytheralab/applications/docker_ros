#FROM ros:galactic-ros-base
FROM osrf/ros:galactic-desktop

# optional, if the default user is not "root", you might need to switch to root here and at the end of the script to the original user again.
# e.g.
# USER root

ENV  DISPLAY=host.docker.internal:0.0 LIBGL_ALWAYS_INDIRECT=0

RUN apt-get update && apt-get install -y --no-install-recommends \
        pkg-config \
        libxau-dev \
        libxdmcp-dev \
        libxcb1-dev \
        libxext-dev \
        libx11-dev && \
    rm -rf /var/lib/apt/lists/*

# replace with other Ubuntu version if desired
# see: https://hub.docker.com/r/nvidia/opengl/
COPY --from=nvidia/opengl:1.0-glvnd-runtime-ubuntu16.04 \
  /usr/local/lib/x86_64-linux-gnu \
  /usr/local/lib/x86_64-linux-gnu

# replace with other Ubuntu version if desired
# see: https://hub.docker.com/r/nvidia/opengl/
COPY --from=nvidia/opengl:1.0-glvnd-runtime-ubuntu16.04 \
  /usr/local/share/glvnd/egl_vendor.d/10_nvidia.json \
  /usr/local/share/glvnd/egl_vendor.d/10_nvidia.json

RUN echo '/usr/local/lib/x86_64-linux-gnu' >> /etc/ld.so.conf.d/glvnd.conf && \
    ldconfig && \
    echo '/usr/local/$LIB/libGL.so.1' >> /etc/ld.so.preload && \
    echo '/usr/local/$LIB/libEGL.so.1' >> /etc/ld.so.preload

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

# USER original_user
# install bootstrap tools
RUN apt-get update && apt-get install --no-install-recommends -y \
    ros-galactic-gazebo-* \
	ros-galactic-joint-state-publisher-gui \
	gazebo11 \
	libgazebo11-dev \
	ignition-edifice \
  && rm -rf /var/lib/apt/lists/*


RUN mkdir -p colcon_ws/src && cd $_  && \
	git clone https://github.com/ignitionrobotics/ign_ros2_control.git && \
	git clone https://github.com/ros-controls/ros2_control.git -b 1.3.0


RUN sudo apt-get update && apt-get install -y --no-install-recommends python3-pip python3-setuptools && \
	pip3 install --upgrade pip cmake pytest pyserial pandas

RUN apt-get update && \
	apt-get install -y --no-install-recommends \
	git \
	software-properties-common \
	make \
	wget \
	vim \
	python3-dev \
	g++ cmake lcov
	
RUN add-apt-repository ppa:ubuntu-toolchain-r/test && \	
	apt-get install -y --no-install-recommends gcc-10 g++-10 gcc-10-multilib g++-10-multilib	 && \
	apt-get install -y --no-install-recommends gcc-11 g++-11 gcc-11-multilib g++-11-multilib	 && \
	update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 60 --slave /usr/bin/g++ g++ /usr/bin/g++-10 && \
	update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-11 50 --slave /usr/bin/g++ g++ /usr/bin/g++-11
		

CMD [ "/bin/bash" ]