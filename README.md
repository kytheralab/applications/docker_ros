#### ROS Aura dockerfiles

This repository contains Dockerfiles and [Docker images](https://gitlab.com/project_gara/applications-software/ros_aura/container_registry) for the ROS versions used in Aura projects

## Getting started (using docker compose v2)

You may donwload the __docker-compose.yml__ file in a repository of your choice:

And the build the docker-compose 
```docker compose up```
If you need to make sure the images used are rebuilt:
```docker-compose up --build```

And run it :
```docker compose run --rm ros2_env```

You may launch another instance with : 

```docker compose exec ros2_env /bin/bash```


To list all dockers:
`sudo docker containter ls -a`  
To delete a docker :
`docker rm Rolling-full-gz-garden`





## Getting acquainted with docker (without using docker composer)
### Install
In order to download the docker image locally you will need to login to the gitlab docker registry with the following :
```shell
sudo docker login registry.gitlab.com -u <user_name> -p <gitlab-token>
```
To generate a gitlab token, go to your gitlab profile. See blogpost [here](https://blog.stephane-robert.info/post/gitlab-container-docker-registry/) for inspiration

### Build the image
To build the image go in the repository containing your image. For example rolling-full-gz-harmonic and type in terminal:
```docker build -t rolling-full-gz-harmonic -f Dockerfile.rolling-full-gz-harmonic .```

### Running the ros docker

To ensure ROS is working in your system, you can use a simple talker/listener dual ROS2 node demo, thus you can launch in two different terminals:  
```shell
sudo docker run -it --rm registry.gitlab.com/project_gara/applications-software/ros_aura/galactic:latest ros2 run demo_nodes_cpp talker
```

```shell
sudo docker run -it --rm registry.gitlab.com/project_gara/applications-software/ros_aura/galactic:latest ros2 run demo_nodes_cpp listener
```

note: `--rm` means the image instance will be cleaned-up on exit.  
Leave that out if you intend to e.g. save what you install in the image and instead use the `--name` argument to name your session.

Caution: if you want to launch your own talker/listener using docker, you have to be in the same container.

Imagine that you are in your container in a terminal. Now open an other terminal, and run the following command:

```sudo docker exec -it <container_ID> bash```


### Running the ros_aura dockers

Build the docker from the dockerfile folder
```sudo docker build -t humble-full-gz-classic -f Dockerfile.humble-full-gz-classic .```
or
```sudo docker build -t rolling-full-gz-garden -f Dockerfile.rolling-full-gz-garden .```


#### Example to run to run Humble image on Win11 + WSL 
Rn the docker with X11 forwarding, access to vGPU and USB, and bash history sharing : 
```docker run -it --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /usr/lib/wsl:/usr/lib/wsl --device=/dev/dxg --gpus all -v /dev:/dev --privileged -v /wsl.localhost/Ubuntu/home/luciza/.docker_bash_history:/root/.bash_history -v /wsl.localhost/Ubuntu/home/luczia/humble_ws/:/colcon_ws -w /colcon_ws --name Humble-full-gz-garden humble-full-gz-garden```
or
```docker run -it --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /usr/lib/wsl:/usr/lib/wsl --device=/dev/dxg --gpus all -v /dev:/dev --privileged -v /wsl.localhost/Ubuntu/home/luciza/.docker_bash_history:/root/.bash_history -v /wsl.localhost/Ubuntu/home/luczia/rolling_ws/:/colcon_ws -w /colcon_ws --name Rolling-full-gz-garden rolling-full-gz-garden```

If you want to start the docker (if you didn't use the --rm option) :
```sudo docker start -ai Humble-full-gz-garden```

Create a new bash connecting to a running container :
```docker exec -it Humble-full-gz-garden/bin/bash```

### Example to run the Rolling image in Ubuntu:
```docker run -it --rm -e DISPLAY=:1 --gpus all -v /tmp/.X11-unix:/tmp/.X11-unix -v /dev:/dev --privileged -v ~/.docker_bash_history:/root/.bash_history -v ~/ros2_ws:/colcon_ws -v ~/patch_ws:/patch_ws -w /colcon_ws --name Rolling-full-gz-garden rolling-full-gz-garden```

```sudo docker exec -it Rolling-full-gz-garden bash```





### Running a shell with access to local files
Use those docker arguments to make your local filesystem visible in the docker session:  
`-v $PWD:/ws`  
that maps $PWD (current directory, the one you run docker from) to /ws in docker.  
`-w /ws`  
indicates /ws as the directory you will land in when the docker session is started.

```shell
sudo docker run -it -v $PWD:/ws -w /ws --rm -e DISPLAY=$DISPLAY registry.gitlab.com/project_gara/applications-software/ros_aura/galactic:latest
```

## Running UI Applications (e.g. Gazebo)

#### Windows/WSL 
you need to have an X server running and use an additional command line argument:  
 `-e DISPLAY=$DISPLAY`
 to properly forward the UI.  
[See here for examples](https://stackoverflow.com/a/61110604/4249338) on how to setup and run an X server to forward to.  

gazebo classic:
```shell
sudo docker run -it --rm -e DISPLAY=$DISPLAY registry.gitlab.com/project_gara/applications-software/ros_aura/galactic:latest gazebo
```

gazebo garden:
```shell
sudo docker run -it --rm -e DISPLAY=$DISPLAY registry.gitlab.com/project_gara/applications-software/ros_aura/humble:latest gz sim
```


#### Ubuntu
1. before launching docker run that:  
`xhost + local:docker`  # grant docker permission to display 

2. Add the following argument to the command line:
`--net=host --device=/dev/dri:/dev/dri`
so that the full command becomes:  

```shell
sudo docker run -it --rm --net=host --device=/dev/dri:/dev/dri -e DISPLAY=$DISPLAY registry.gitlab.com/project_gara/applications-software/ros_aura/galactic:latest gazebo
```

Caution: If you want to launch an executable and give access to serial devices in docker, use the following command (share the /dev volume): 

```sudo docker run -it --rm --net=host --privileged -v /dev/bus/usb:/dev/bus/usb -e DISPLAY=$DISPLAY <image_name>```

3. You can avoid using the sudo command by adding the docker group to the 

```sudo groupadd docker```
```sudo gpasswd -a $USER docker```
And restart the session (or reboot the computer)



## Commnication between docker and host

The dokcer are configured to comunication through the host network ips and using the Cyclone DDS.
You may ensure your ROS environvment uses the CycloneDDS with :

`echo $RMW_IMPLEMENTATION`  shoud output: `rmw_cyclonedds_cpp ` if not, you may want to export it : `export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp` on both the docker and the host.
## Useful docker commands:  

- the `-it` args to docker run allow to have an interactive terminal session that can be ended with CTRL-C:  
 ```sudo docker run -it <some_image> /bin/bash```

- To list local images:  
```docker images```

- See the list of previously run sessions:  
```docker ps -a```

- To remove previously run containers you don't want to keep around:  
```docker container prune```


- Give a name to a container with the `--name=<some_name>` argument:  
```docker create --name=<some_name> <some_image_url>```  
or  
```docker run --name=<some_name> <some_image_url> <some_command>```  
 so that you can reuse it easily with:  
```docker restart <some_name>``` # runs the docker  
```docker attach <some_name>``` # attach it to current terminal  

- To save changes made in a container:
```docker commit <container_ID> <new_image_name>```

- To move a file from host machine to a container:
```docker cp file.txt <container_ID>:/to_the_place_you_want_the_file_to_be```

- Persist the bash history:  
either:
    - create a `~/.docker_bash_history` file in your home  

```shell
docker run -it -v ~/.docker_bash_history:/root/.bash_history <...>
```
    - or reuse your local file history:  
```shell
docker run -it -v ~/.bash_history:/root/.bash_history <...>
```

## References
for ros + docker + gazebo (UI) install  :
An example docker file for gazebo and ros kinetic:
https://github.com/Seanmatthews/ros-docker-gazebo/blob/master/kinetic/Dockerfile

Despite what it says there:  
https://docs.microsoft.com/en-us/windows/wsl/tutorials/gui-apps
It seems installing VcXsrv is still required.
